# Astro React Docker Base

The repository contains a base for a dockerized version of a Astro app with React. The repository can be used as a base to develop a Astro app with react without the need to install any additional tools.

## Run locally for development

`> make app-dev`

OR

`> docker compose up --remove-orphans app-dev`

## Deploy and Run in Production

`> make app`

OR

`> docker compose --remove-orphans app`

## Requirments and Dependencies

1. Docker and Docker Compose is installed and configured.
    * [Docker](https://docs.docker.com/install/)
    * [Docker Compose](https://docs.docker.com/compose/install/)

2. Find and replace `astro-react-docker-base` to an appropriate application name.

## Credits and References

1. [Build your Astro Site with Docker](https://docs.astro.build/en/recipes/docker/)

2. [Migrating from Create React App (CRA)](https://docs.astro.build/en/guides/migrate-to-astro/from-create-react-app/)
