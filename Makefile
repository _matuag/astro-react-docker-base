# include .env

DC=docker compose
# make action arguments
CMD=$(filter-out $@,$(MAKECMDGOALS))

app-dev:
	$(DC) up --remove-orphans app-dev

app:
	$(DC) up --remove-orphans app

stop:
	$(DC) stop

